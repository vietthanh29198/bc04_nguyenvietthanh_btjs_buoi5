function ketQua() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemMon1 = document.getElementById("txt-diem-mon-1").value * 1;
  var diemMon2 = document.getElementById("txt-diem-mon-2").value * 1;
  var diemMon3 = document.getElementById("txt-diem-mon-3").value * 1;
  var khuVuc = document.getElementById("txt-khu-vuc").value;
  var doiTuong = document.getElementById("txt-doi-tuong").value;

  var tongDiem = diemMon1 + diemMon2 + diemMon3;

  switch (khuVuc) {
    case "A":
      tongDiem += 2;
      break;

    case "B":
      tongDiem += 1;
      break;

    case "C":
      tongDiem += 0.5;
      break;
  }

  switch (doiTuong) {
    case "1":
      tongDiem += 2.5;
      break;

    case "2":
      tongDiem += 1.5;
      break;

    case "3":
      tongDiem += 1;
      break;
  }

  if (tongDiem >= diemChuan) {
    document.getElementById(
      "result"
    ).innerHTML = `Bạn đã đậu. Tổng điểm: ${tongDiem}`;
  } else {
    document.getElementById(
      "result"
    ).innerHTML = `Bạn đã rớt. Tổng điểm: ${tongDiem}`;
  }
}
