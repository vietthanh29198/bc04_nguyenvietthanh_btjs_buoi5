function tinhTienDien() {
  var hoTen = document.getElementById("txt-ho-ten").value;
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var result = 0;

  if (soKw <= 50) {
    result = soKw * 500;
  } else if (soKw > 50 && soKw <= 100) {
    result = 50 * 500 + (soKw - 50) * 650;
  } else if (soKw > 100 && soKw <= 200) {
    result = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
  } else if (soKw > 200 && soKw <= 350) {
    result = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 150) * 1100;
  } else {
    result = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
  }

  document.getElementById(
    "result"
  ).innerHTML = `Họ tên: <span class ="display-4 text-success">${hoTen}</span> ; Tiền điện: <span class="display-4 text-danger">${result.toLocaleString(
    "vn-VN"
  )} VNĐ</span>`;
}
